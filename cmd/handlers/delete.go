package handlers

import (
	"database/sql"
	"ternotes/cmd/checks"
	"ternotes/cmd/out"
)

func DeleteID(id int, db *sql.DB) {

	sql := "DELETE FROM notes WHERE id = ?"

	// выполним SQL запрос
	stmt, err := db.Prepare(sql)
	// выход при ошибке
	checks.CheckErrors(err)

	// заменим символ '?' в запросе на 'id'
	result, err2 := stmt.Exec(id)
	// выход при ошибке
	checks.CheckErrors(err2)
	s, err := result.RowsAffected()
	checks.CheckErrors(err)
	out.ShowDeletedID(s)
}

func DeleteTAG(tag string, db *sql.DB) {

	sql := "DELETE FROM notes WHERE tag = ?"

	// выполним SQL запрос
	stmt, err := db.Prepare(sql)
	// выход при ошибке
	checks.CheckErrors(err)

	// заменим символ '?' в запросе на 'id'
	result, err2 := stmt.Exec(tag)
	// выход при ошибке
	checks.CheckErrors(err2)
	s, err := result.RowsAffected()
	checks.CheckErrors(err)
	out.ShowDeletedTag(s)
}
