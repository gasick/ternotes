package env

const HelloText = `To note something use next command:
	ternotes add TAG

To list notes use:
	ternotes list - will show TAGS
	or
	ternotes list TAG - will show notes in TAG

To remove note  use:
	ternotes delete noteID

You can rename ternotes as you wish and use a new name.
`
